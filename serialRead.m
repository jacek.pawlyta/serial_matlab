% Matlab R2019b code to demonstrate data read from n instrument
% 
% licence: GPL 2.0
% author: Jacek Pawlyta
% date: 22.11.2021
% version: 2.1
% changes: new serial interface of Matlab used



clear;

% initialise communication window
% 
ButtonHandle = uicontrol('Style', 'PushButton', ...
                         'String', 'Stop loop', ...
                         'Callback', 'delete(gcbf)');

% initialise serial port for matlab (Linux users please make sure you gave
% write permissions to /lock directory (STRANGE!!)
mySerialPort = serialport("/dev/ttyUSB1",9600); % define serial port

% endless loop to read data line from serial port and print it in Matlab
% console

while(true)
    if ~ishandle(ButtonHandle)
        disp('Loop stopped by user');
        break;
    end    
    myData = readline(mySerialPort);
    disp(myData)
end


